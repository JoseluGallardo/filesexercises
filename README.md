#Ejercicios Ficheros José Luis del Pino Gallardo para ACDAT

* Version 1.0
* Min SDK: 22

#Menú Principal
**Al pulsar cada botón lleva a una actividad concreta.**

Vista del menú:

![Screenshot_1479668219.png](https://bitbucket.org/repo/kpr9pp/images/3346320740-Screenshot_1479668219.png)



#1.- Agenda
**Muestra una lista de contactos. Nos permite añadir más contactos y los guarda en XML.**

-Vista de añadir un nuevo contacto:

#![Screenshot_1479668321.png](https://bitbucket.org/repo/kpr9pp/images/2968263522-Screenshot_1479668321.png)

-Vista de la agenda con contactos:

![Screenshot_1479668328.png](https://bitbucket.org/repo/kpr9pp/images/3317975953-Screenshot_1479668328.png)

#2.- Alarmas
**Programa 5 alarmas, con un intervalo y mensaje personalizado.**

-Vista principal:

![Screenshot_1479668456.png](https://bitbucket.org/repo/kpr9pp/images/2980234421-Screenshot_1479668456.png)

-Cuando acaba una alarma:

![Screenshot_1479668520.png](https://bitbucket.org/repo/kpr9pp/images/783398919-Screenshot_1479668520.png)

#3.- MenstruApp
**Calcula los días fértiles de una mujer, pidiendo el inicio de su último período y el ciclo de éste**

-Seleccionamos el día en el calendario:

![Screenshot_1479668622.png](https://bitbucket.org/repo/kpr9pp/images/2135045453-Screenshot_1479668622.png)

-Seleccionamos la duración media del ciclo:

![Screenshot_1479668628.png](https://bitbucket.org/repo/kpr9pp/images/372809884-Screenshot_1479668628.png)

-Y vemos señalado en el calendario los días fértiles. Si coincide con hoy, avisa:

![Screenshot_1479668639.png](https://bitbucket.org/repo/kpr9pp/images/1200342478-Screenshot_1479668639.png)

#4.- Navegador
**Permite conexión a internet cargando la página en un WebView mediante los métodos de JAVA, AAHC y Volley **

-Vista del navegador:

![Screenshot_1479668810.png](https://bitbucket.org/repo/kpr9pp/images/4174244541-Screenshot_1479668810.png)

#5.- Descarga y galería de imágenes:
**Muestra una galería predefinida de imágenes, y permite descargar más añadiéndolas a la galería**

-Imagen de la galería predefinida:

![Screenshot_1479668824.png](https://bitbucket.org/repo/kpr9pp/images/1347226835-Screenshot_1479668824.png)

-Descargamos otra imagen:

![Screenshot_1479668828.png](https://bitbucket.org/repo/kpr9pp/images/2115211214-Screenshot_1479668828.png)

#6.- Conversar de monedas:
**Convierte dólares a euros y viceversa usando como valor de conversión un fichero en la web**

-Vista del conversor:

![Screenshot_1479668844.png](https://bitbucket.org/repo/kpr9pp/images/1337728394-Screenshot_1479668844.png)

#7.- Subidor de archivos:
**Sube archivos desde el móvil a un servidor remoto**

-Vista de un archivo subido correctamente:

![Screenshot_1479668898.png](https://bitbucket.org/repo/kpr9pp/images/4138498446-Screenshot_1479668898.png)

-Vista de un archivo que no se ha podido subir:

![Screenshot_1479668861.png](https://bitbucket.org/repo/kpr9pp/images/4170273980-Screenshot_1479668861.png)

## José Luis del Pino Gallardo ##