package com.jsw.ejerciciosficheros.model;

/**
 * Created by Joselu on 29/10/16.
 */

public class Contacto {

    public Contacto(int Image, String Name, String Number, String Mail){
        this.setImagen(Image);
        this.setNombre(Name);
        this.setNumero(Number);
        this.setMail(Mail);
    }

    private int imagen;
    private String nombre;
    private String mail;
    private String numero;

    public int getImagen() {
        return imagen;
    }

    public void setImagen(int imagen) {
        this.imagen = imagen;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }
}
