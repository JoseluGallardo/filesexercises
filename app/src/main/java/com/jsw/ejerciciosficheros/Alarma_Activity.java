package com.jsw.ejerciciosficheros;

import android.app.Application;
import android.media.MediaPlayer;
import android.net.sip.SipAudioCall;
import android.os.CountDownTimer;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.util.Date;

public class Alarma_Activity extends AppCompatActivity {

    EditText mIntervalo;
    EditText mMensaje;
    Button mEmpezar;
    CountDownTimer mCount;
    TextView mCrono;
    TextView mAlarmas;
    int mTime;
    long mLongTime;
    int nAlarma = 0;
    File testDirectory;
    File file;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alarma);
        mIntervalo = (EditText)findViewById(R.id.edt_intervalo);
        mMensaje = (EditText)findViewById(R.id.edt_mensaje);
        mEmpezar = (Button)findViewById(R.id.btn_empezar);
        mCrono = (TextView)findViewById(R.id.chronometer3);
        mAlarmas = (TextView)findViewById(R.id.textView5);
        testDirectory = Environment.getExternalStorageDirectory();
        file = new File(testDirectory + "/" + "alarmas.txt");


        mEmpezar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(!TextUtils.isEmpty(mIntervalo.getText()) && !TextUtils.isEmpty(mMensaje.getText())) {
                    mEmpezar.setEnabled(false);
                    mTime = Integer.parseInt(mIntervalo.getText().toString());
                    mLongTime = mTime * 60 * 1000;

                    guardar();
                    sonar(mLongTime);
                }else
                    Toast.makeText(getApplicationContext(), "Existen valores vacios.", Toast.LENGTH_LONG).show();
            }
        });
    }

    private void guardar(){
            BufferedWriter bw;

            try{

                file.createNewFile();

                bw = new BufferedWriter(new FileWriter(testDirectory + "/" + file.getName()));

                for (int i = 1; i < 6; i++){
                    bw.append("Alarma: " + i + ". Duración: " + mIntervalo.getText().toString() + ". Mensaje: " +   mMensaje.getText().toString());
                    bw.newLine();
                    bw.flush();
                }

                if (bw != null)
                    bw.close();
            }

            catch (Exception e) {
                Log.e("ERROR WRITING", "Unable to write" + e.getMessage());
            }
        }

    private void sonar(long t){

        mCrono.setText(DateFormat.format("mm:ss", t));

        mCount = new CountDownTimer(t, 1000) {
            @Override
            public void onTick(long l) {
                mCrono.setText(DateFormat.format("mm:ss", l));
            }

            @Override
            public void onFinish() {
                mCrono.setText("00:00");
                Toast.makeText(getApplicationContext(), "Fin alarma " + ++nAlarma + "   Mensaje: " + mMensaje.getText().toString(), Toast.LENGTH_LONG).show();
                mAlarmas.setText("Alarmas finalizadas: " + nAlarma + "/5");

                if(nAlarma < 5) {
                    mCrono.setText(DateFormat.format("mm:ss", mLongTime));
                    mCount.start();
                }else
                    mEmpezar.setEnabled(true);
            }
        };

        mCount.start();
    }

    @Override
    protected void onPause() {
        super.onPause();
        Toast.makeText(this, "Alarmas Canceladas", Toast.LENGTH_LONG).show();
        if(mCount != null)
            mCount.cancel();
    }
}
