package com.jsw.ejerciciosficheros;

import android.content.Intent;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.google.gson.Gson;
import com.jsw.ejerciciosficheros.model.Contacto;

public class AddContact_Activity extends AppCompatActivity {

    TextInputLayout mTilName, mTilPhone, mTilMail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_contact);
        mTilName = (TextInputLayout) findViewById(R.id.til_nombre);
        mTilPhone = (TextInputLayout) findViewById(R.id.til_phone);
        mTilMail = (TextInputLayout) findViewById(R.id.til_mail);
    }

    public void guardar(View v){
        try{
            Intent i = getIntent();
            Contacto c = new Contacto(
                    R.mipmap.ic_launcher,
                    mTilName.getEditText().getText().toString(),
                    mTilPhone.getEditText().getText().toString(),
                    mTilMail.getEditText().getText().toString());

            i.putExtra("Contacto", new Gson().toJson(c));
            setResult(RESULT_OK, i);
            finish();
        } catch (Exception ex){
            Toast.makeText(this, "Error al añadir", Toast.LENGTH_LONG);
        }


    }
}
