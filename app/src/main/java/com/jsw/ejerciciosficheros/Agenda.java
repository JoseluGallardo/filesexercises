package com.jsw.ejerciciosficheros;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.google.gson.Gson;
import com.jsw.ejerciciosficheros.adapter.adapterAgenda;
import com.jsw.ejerciciosficheros.model.Contacto;

public class Agenda extends ListActivity {

    adapterAgenda adapter_c;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.agenda_activity);
        adapter_c = new adapterAgenda(this);
        getListView().setAdapter(adapter_c);
    }

    public void añadir(View v){
        Intent intent = new Intent(this, AddContact_Activity.class);
        startActivityForResult(intent, 0);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // Check which request we're responding to
        if (requestCode == 0) {
            // Make sure the request was successful
            if (resultCode == RESULT_OK) {
                String jsonMyObject = "";
                Bundle extras = data.getExtras();
                if (extras != null) {
                    jsonMyObject = extras.getString("Contacto");
                }
                Contacto contacto = new Gson().fromJson(jsonMyObject, Contacto.class);
                adapter_c.añadir(contacto);
                adapter_c.notifyDataSetChanged();
            }
        }
    }
}
