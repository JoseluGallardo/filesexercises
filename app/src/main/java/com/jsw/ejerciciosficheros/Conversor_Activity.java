package com.jsw.ejerciciosficheros;

import android.os.Environment;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;

public class Conversor_Activity extends AppCompatActivity {

    private EditText eurBox;
    private EditText usdBox;
    private RadioButton eurAusd;
    private Conversor convierte;
    private Double resultado;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_conversor);
        eurBox = (EditText)findViewById(R.id.edtxt_eur);
        usdBox = (EditText)findViewById(R.id.edtxt_usd);
        eurAusd = (RadioButton)findViewById(R.id.rbtn_eurAUsd);
        convierte = new Conversor();
        resultado = 0.0;
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
    }

    public void convertir(View v){

        if(eurAusd.isChecked()) { //Si está checkeado
            if(eurBox.getText().toString() != ""){ //y tu texti es distinto de vacio
                try {
                    resultado = convierte.EurAUsd(Double.parseDouble(eurBox.getText().toString())); //Guarda en Resultado el double devuelto en el método
                    usdBox.setText(resultado.toString()); //Y actualiza el texto
                }catch (Exception ex){
                }
            }
        }

        else if (usdBox.getText().toString() != ""){ //Si no está marcado, y los dolares están distintos de 0
            try {
                resultado = convierte.UsdAEur(Double.parseDouble(usdBox.getText().toString())); //Lo mismo
                eurBox.setText(resultado.toString());
            }catch (Exception ex){
            }
        }
    }
}

final class Conversor{


    public double EurAUsd(double euros){

        download("https://acta.000webhostapp.com/EUR.txt", "euros.txt");

        File sdcard = Environment.getExternalStorageDirectory();

        //Get the text file
        File file = new File(sdcard,"euros.txt");

        //Read text from file
        StringBuilder text = new StringBuilder();

        try {
            BufferedReader br = new BufferedReader(new FileReader(file));
            String line;

            while ((line = br.readLine()) != null) {
                text.append(line);
                text.append('\n');
            }
            br.close();
        }
        catch (IOException e) {
            //You'll need to add proper error handling here
        }

        return Double.parseDouble(text.toString()) * euros;
    }

    public double UsdAEur(double usd){
        download("https://acta.000webhostapp.com/USD.txt", "usd.txt");

        File sdcard = Environment.getExternalStorageDirectory();

        //Get the text file
        File file = new File(sdcard,"usd.txt");

        //Read text from file
        StringBuilder text = new StringBuilder();

        try {
            BufferedReader br = new BufferedReader(new FileReader(file));
            String line;

            while ((line = br.readLine()) != null) {
                text.append(line);
                text.append('\n');
            }
            br.close();
        }
        catch (IOException e) {
            //You'll need to add proper error handling here
        }

        return Double.parseDouble(text.toString()) * usd;
    }

    private void download(String link, String name){
        try {
            URL url = new URL(link);
            URLConnection conexion = url.openConnection();
            conexion.connect();
            int lenghtOfFile = conexion.getContentLength();
            InputStream is = url.openStream();
            File testDirectory = Environment.getExternalStorageDirectory();
            /*if (!testDirectory.exists()) {
                testDirectory.mkdir();
            }*/
            File file = new File(testDirectory + "/" + name);
            if (!file.exists())
                file.createNewFile();

            FileOutputStream fos = new FileOutputStream(testDirectory + "/"+name);
            byte data[] = new byte[1024];
            long total = 0;
            int count = 0;
            while ((count = is.read(data)) != -1) {
                total += count;
                int progress_temp = (int) total * 100 / lenghtOfFile;
                fos.write(data, 0, count);
            }
            is.close();
            fos.close();
        } catch (Exception e) {
            Log.e("ERROR DOWNLOADING", "Unable to download" + e.getMessage());
        }
    }

}