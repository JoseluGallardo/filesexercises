package com.jsw.ejerciciosficheros;

import android.media.Image;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import static android.R.id.list;

public class Descarga_Imagenes_Activity extends AppCompatActivity {

    private TextInputLayout mUrl;
    private ImageView mImagen;
    private String mLink;
    private List<String> images;
    private int flag = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_descarga_imagenes);
        //http://i.imgur.com/hlWzRAQ.jpg
        mUrl = (TextInputLayout) findViewById(R.id.til_url);
        mImagen = (ImageView) findViewById(R.id.iv_imagen);
        images = new ArrayList<>(2);
        images.add("https://i.imgur.com/RGzP6lk.jpg");
        images.add("https://i.imgur.com/quafTy7.jpg");
    }

    private void picasso(String link){
        Picasso.with(getApplicationContext()).load(link) .placeholder(R.drawable.wait) .error(R.drawable.error)
        .resize(300, 300).into(mImagen);
    }

    public void descargar(View v){
        mLink = mUrl.getEditText().getText().toString();

        if(!TextUtils.isEmpty(mLink)) {
            picasso(mLink);
            images.add(mLink);
            Toast.makeText(this, "Imagen descargada correctamente.", Toast.LENGTH_SHORT).show();
        }
    }

    public void avanzar(View v){
        String nextImage = images.get(flag++ % images.size());
        picasso(nextImage);
    }

    public void retroceder (View v){
        String nextImage = images.get(flag++ % images.size());
        picasso(nextImage);
    }
}
