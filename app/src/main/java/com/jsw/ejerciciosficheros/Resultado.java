package com.jsw.ejerciciosficheros;

/**
 * Created by Joselu on 19/11/16.
 */

public class Resultado {

    private boolean codigo;
    private String mensaje;
    private String contenido;

    public boolean isCodigo() {
        return codigo;
    }

    public String getMensaje() {
        return mensaje;
    }

    public String getContenido() {
        return contenido;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public void setContenido(String contenido) {
        this.contenido = contenido;
    }

    public void setCodigo(boolean codigo) {

        this.codigo = codigo;
    }

    public Resultado(boolean codigo, String mensaje, String contenido) {
        this.codigo = codigo;
        this.mensaje = mensaje;
        this.contenido = contenido;
    }

    public Resultado() {

    }
}
