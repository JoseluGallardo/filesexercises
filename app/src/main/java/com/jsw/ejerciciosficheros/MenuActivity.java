package com.jsw.ejerciciosficheros;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class MenuActivity extends AppCompatActivity {

    Intent i;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
    }

    public void alarma(View v){
        i = new Intent(this, Alarma_Activity.class);
        startActivity(i);
    }

    public void menstruApp(View v){
        i = new Intent(this, MenstruApp_Activity.class);
        startActivity(i);
    }

    public void descargaImg(View v){
        i = new Intent(this, Descarga_Imagenes_Activity.class);
        startActivity(i);
    }

    public void red(View v){
        i = new Intent(this, Browser_Activity.class);
        startActivity(i);
    }

    public void conversor(View v){
        i = new Intent(this, Conversor_Activity.class);
        startActivity(i);
    }

    public void agenda(View v){
        i = new Intent(this, Agenda.class);
        startActivity(i);
    }

    public void subir(View v){
        i = new Intent(this, Upload_Activity.class);
        startActivity(i);
    }
}
