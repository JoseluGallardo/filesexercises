package com.jsw.ejerciciosficheros;

/**
 * Created by joselu on 16/11/16.
 */

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewParent;
import android.widget.DatePicker;

public class MiDatePicker extends DatePicker {

    public MiDatePicker(Context context) {
        super(context);
    }

    public MiDatePicker(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public MiDatePicker(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        ViewParent parentView = getParent();

        if (ev.getActionMasked() == MotionEvent.ACTION_DOWN) {
            if (parentView != null) {
                parentView.requestDisallowInterceptTouchEvent(true);
            }
        }

        return false;
    }
}
