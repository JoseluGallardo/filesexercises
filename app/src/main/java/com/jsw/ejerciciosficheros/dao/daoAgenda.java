package com.jsw.ejerciciosficheros.dao;

import android.app.Application;
import android.os.Environment;
import android.util.Log;
import android.util.Xml;

import com.jsw.ejerciciosficheros.R;
import com.jsw.ejerciciosficheros.model.Contacto;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlSerializer;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Joselu on 29/10/16.
 */

public class daoAgenda extends Application {

    // We don't use namespaces
    private static final String ns = null;
    static List<Contacto> mContactos;
    FileInputStream in;

    public List<Contacto> getContacts(){
        return this.mContactos;
    }

    @Override
    public void onCreate(){
        try{
            crear();
            this.mContactos = new ArrayList<Contacto>();
            this.mContactos.addAll(parsear());
        } catch (Exception ex) {

        }
    }

    public void crear(){
        File newxmlfile = new File(Environment.getExternalStorageDirectory() + "/agenda.xml");
        try{
            newxmlfile.createNewFile();
        }catch(IOException e)
        {
            Log.e("IOException", "Exception in create new File(");
        }
        FileOutputStream fileos = null;
        try{
            fileos = new FileOutputStream(newxmlfile);

        }catch(FileNotFoundException e)
        {
            Log.e("FileNotFoundException",e.toString());
        }
        XmlSerializer serializer = Xml.newSerializer();
        try{
            serializer.setOutput(fileos, "UTF-8");
            serializer.startDocument(null, Boolean.valueOf(true));
            serializer.setFeature("http://xmlpull.org/v1/doc/features.html#indent-output", true);
            serializer.startTag(null, "agenda");
            serializer.startTag(null, "contact");

            serializer.startTag(null, "image");
            serializer.text(String.valueOf(R.mipmap.ic_launcher));
            serializer.endTag(null, "image");

            serializer.startTag(null, "name");
            serializer.text("Joselu");
            serializer.endTag(null, "name");

            serializer.startTag(null, "phone");
            serializer.text("622301904");
            serializer.endTag(null, "phone");

            serializer.startTag(null, "mail");
            serializer.text("@joselu.com");
            serializer.endTag(null, "mail");

            serializer.endTag(null,"contact");

            serializer.startTag(null, "contact");

            serializer.startTag(null, "image");
            serializer.text(String.valueOf(R.mipmap.ic_launcher));
            serializer.endTag(null, "image");

            serializer.startTag(null, "name");
            serializer.text("Airora");
            serializer.endTag(null, "name");

            serializer.startTag(null, "phone");
            serializer.text("9532346");
            serializer.endTag(null, "phone");

            serializer.startTag(null, "mail");
            serializer.text("@auroa.com");
            serializer.endTag(null, "mail");

            serializer.endTag(null,"contact");
            serializer.endTag(null, "agenda");

            serializer.endDocument();
            serializer.flush();
            fileos.close();
            //TextView tv = (TextView)findViewById(R.);

        }catch(Exception e)
        {
            Log.e("Exception","Exception occured in wroting");
        }
    }

    public void addAndWrite(Contacto c){
        mContactos.add(c);
        try{
            write(c);
        }catch(Exception ex){
            Log.e("ERROR", ex.getMessage());
        }
    }

    public void write(Contacto c) throws IOException{
        try {
            StringWriter writer2;

            String my_file_path = "/data/data/com.jsw.acdat_agenda/files/agenda.xml";
            RandomAccessFile raf = new RandomAccessFile(my_file_path, "rw");
            raf.seek((my_file_path.length()) -1);

            XmlSerializer xmlSerializer5 = Xml.newSerializer();
            writer2 = new StringWriter();
            xmlSerializer5.setOutput(writer2);
            xmlSerializer5.setFeature("http://xmlpull.org/v1/doc/features.html#indent-output", true);

            xmlSerializer5.startTag(null, "contact");

            xmlSerializer5.startTag(null, "image");
            xmlSerializer5.text(getString(c.getImagen()));
            xmlSerializer5.endTag(null, "image");

            xmlSerializer5.startTag(null, "name");
            xmlSerializer5.text(c.getNombre());
            xmlSerializer5.endTag(null, "name");

            xmlSerializer5.startTag(null, "phone");
            xmlSerializer5.text(c.getNumero());
            xmlSerializer5.endTag(null, "phone");

            xmlSerializer5.startTag(null, "mail");
            xmlSerializer5.text(c.getMail());
            xmlSerializer5.endTag(null, "mail");

            xmlSerializer5.endTag(null, "contact");
            xmlSerializer5.flush();

            String my_dataWrite = writer2.toString();
            raf.write(my_dataWrite.getBytes());
            raf.close();
        } finally {
            in.close();
        }
    }


    /**
     * Parsea un flujo XML a una lista de objetos
     *
     * @return Lista de hoteles
     * @throws XmlPullParserException
     * @throws IOException
     */
    public List<Contacto> parsear() throws XmlPullParserException, IOException {
        try {
            in = new FileInputStream("/data/data/com.jsw.acdat_agenda/files/agenda.xml");
            XmlPullParser parser = Xml.newPullParser();
            parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
            parser.setInput(in, null);
            parser.nextTag();
            return leerHoteles(parser);
        } finally {
            in.close();
        }
    }

    /**
     * Convierte una serie de etiquetas <hotel> en una lista
     *
     * @param parser
     * @return lista de hoteles
     * @throws XmlPullParserException
     * @throws IOException
     */
    private List<Contacto> leerHoteles(XmlPullParser parser) throws XmlPullParserException, IOException {
        List<Contacto> interna = new ArrayList<Contacto>();
        parser.require(XmlPullParser.START_TAG, ns, "agenda");
        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.getEventType() != XmlPullParser.START_TAG) {
                continue;
            }
            String nombreEtiqueta = parser.getName();
            // Buscar etiqueta <hotel>
            if (nombreEtiqueta.equals("contact")) {
                interna.add(leerContacto(parser));
            } else {
                saltarEtiqueta(parser);
            }
        }
        return interna;
    }

    /**
     * Convierte una etiqueta <hotel> en un objero Hotel
     *
     * @param parser parser XML
     * @return nuevo objeto Hotel
     * @throws XmlPullParserException
     * @throws IOException
     */
    private Contacto leerContacto(XmlPullParser parser) throws XmlPullParserException, IOException {
        parser.require(XmlPullParser.START_TAG, ns, "contact");
        String id = "";
        String nombre = "";
        String phone = "";
        String mail = "";
        try {

            while (parser.next() != XmlPullParser.END_TAG) {
                if (parser.getEventType() != XmlPullParser.START_TAG) {
                    continue;
                }
                String name = parser.getName();

                switch (name) {
                    case "name":
                        nombre = leerNombre(parser);
                        break;
                    case "phone":
                        phone = leerNum(parser);
                        break;
                    case "mail":
                        mail = leerMail(parser);
                        break;
                    default:
                        saltarEtiqueta(parser);
                        break;
                }
            }

        } catch (Exception ex){
            Log.e("DAO", ex.getMessage());
        }
        return new Contacto(R.id.iv_photo, nombre, phone, mail);
    }

    // Procesa la etiqueta <NOMBRE> de los hoteles
    private String leerNombre(XmlPullParser parser) throws IOException, XmlPullParserException {
        parser.require(XmlPullParser.START_TAG, ns, "name");
        String idHotel = obtenerTexto(parser);
        parser.require(XmlPullParser.END_TAG, ns, "name");
        return idHotel;
    }

    // Procesa las etiqueta <PHONE> de los hoteles
    private String leerNum(XmlPullParser parser) throws IOException, XmlPullParserException {
        parser.require(XmlPullParser.START_TAG, ns, "phone");
        String nombre = obtenerTexto(parser);
        parser.require(XmlPullParser.END_TAG, ns, "phone");
        return nombre;
    }

    // Procesa la etiqueta <MAIL> de los hoteles
    private String leerMail(XmlPullParser parser) throws IOException, XmlPullParserException {
        parser.require(XmlPullParser.START_TAG, ns, "mail");
        String precio = obtenerTexto(parser);
        parser.require(XmlPullParser.END_TAG, ns, "mail");
        return precio;
    }

    // Obtiene el texto de los atributos
    private String obtenerTexto(XmlPullParser parser) throws IOException, XmlPullParserException {
        String resultado = "";
        if (parser.next() == XmlPullParser.TEXT) {
            resultado = parser.getText();
            parser.nextTag();
        }
        return resultado;
    }

    // Salta aquellos objeteos que no interesen en la jerarquía XML.
    private void saltarEtiqueta(XmlPullParser parser) throws XmlPullParserException, IOException {
        if (parser.getEventType() != XmlPullParser.START_TAG) {
            throw new IllegalStateException();
        }
        int depth = 1;
        while (depth != 0) {
            switch (parser.next()) {
                case XmlPullParser.END_TAG:
                    depth--;
                    break;
                case XmlPullParser.START_TAG:
                    depth++;
                    break;
            }
        }
    }
}