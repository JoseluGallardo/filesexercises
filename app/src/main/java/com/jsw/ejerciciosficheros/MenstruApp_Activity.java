package com.jsw.ejerciciosficheros;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;

import com.github.sundeepk.compactcalendarview.domain.Event;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class MenstruApp_Activity extends AppCompatActivity {

    DatePicker mFecha;
    Spinner mCiclo;
    ArrayAdapter adapter;
    String[] dias = {"25","26","27","28","29","30","31"};
    int diaInicial;
    int diaFinal;
    int ciclo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menstru_app_);
        mCiclo = (Spinner) findViewById(R.id.spinner);
        adapter = new ArrayAdapter(this, android.R.layout.simple_spinner_item, dias);
        mCiclo.setAdapter(adapter);
        mFecha = (DatePicker)findViewById(R.id.datePicker);

    }

    public void calcular(View v){
        String[] e = new String[4];
        ciclo = Integer.valueOf((String)mCiclo.getSelectedItem());

        Calendar calendar = Calendar.getInstance();
        int dia = (ciclo/2) - 2;

        calendar.set(mFecha.getYear(), mFecha.getMonth(), mFecha.getDayOfMonth());



        calendar.add(calendar.DATE, dia); // Configuramos la fecha que se recibe
        e[0] = new String(String.valueOf(calendar.getTimeInMillis()));
        calendar.add(Calendar.DAY_OF_MONTH, 1);
        e[1] = new String(String.valueOf(calendar.getTimeInMillis()));
        calendar.add(Calendar.DAY_OF_MONTH, 1);
        e[2] = new String(String.valueOf(calendar.getTimeInMillis()));
        calendar.add(Calendar.DAY_OF_MONTH, 1);
        e[3] = new String(String.valueOf(calendar.getTimeInMillis()));
        calendar.add(Calendar.DAY_OF_MONTH, 1);

        Intent i = new Intent(this, MenstruApp_Mes_Activity.class);
        i.putExtra("events", e);
        startActivity(i);
        finish();
    }
}
