package com.jsw.ejerciciosficheros;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.TextHttpResponseHandler;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import cz.msebera.android.httpclient.Header;

public class Browser_Activity extends AppCompatActivity {
    private EditText mUrl;
    private RadioGroup mRadioGroup;
    private WebView mWebView;
    private LinearLayout mLayout;
    private static final String ENCODING = "UTF-8";
    private static final String CONECTION_JAVA = "JAVA";
    private static final String CONECTION_AAHC = "AAHC";
    private static final String CONECTION_VOLLEY = "VOLLEY";
    private String conectionSelected;
    private boolean mInternetConection;
    private StringBuilder contentWeb;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_browser);
        mLayout = (LinearLayout)findViewById(R.id.activity_browser);
        mUrl = (EditText)findViewById(R.id.editText);
        mRadioGroup = (RadioGroup)findViewById(R.id.rg_options);
        mWebView = (WebView)findViewById(R.id.wv_internet);
        mInternetConection = redDisponible();
        conectionSelected = CONECTION_JAVA;

        mRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {

                switch (checkedId){

                    case R.id.rb_java:
                        conectionSelected = CONECTION_JAVA;
                        break;
                    case R.id.rb_aahc:
                        conectionSelected = CONECTION_AAHC;
                        break;
                    case R.id.rb_volley:
                        conectionSelected = CONECTION_VOLLEY;
                        break;
                }
            }
        });
    }


    //region CONEXIÓN
    public void conectar (View v) {
        if(redDisponible())
            setConexion();
        else
            Snackbar.make(mLayout, "No hay una conexión disponible", Snackbar.LENGTH_LONG).show();
    }

    private void setConexion() {
        switch (conectionSelected){

            case CONECTION_JAVA:
                if(mUrl.getText().toString().isEmpty())
                    Toast.makeText(this, "No hay una ruta establecida", Toast.LENGTH_LONG).show();
                else{

                    if(redDisponible()){

                        ConexionAsync conext = new ConexionAsync(Browser_Activity.this, ENCODING);
                        conext.execute(mUrl.getText().toString());
                    }

                    else
                        Toast.makeText(Browser_Activity.this, "No hay una conexión disponible", Toast.LENGTH_LONG).show();
                }
                break;
            case CONECTION_AAHC:
                if(mUrl.getText().toString().isEmpty())
                    Toast.makeText(Browser_Activity.this, "No hay una ruta establecida", Toast.LENGTH_LONG).show();
                else{

                    if(redDisponible())
                        conectarAAHC();
                    else
                        Toast.makeText(Browser_Activity.this, "No hay una conexión disponible", Toast.LENGTH_LONG).show();
                }

                break;
            case CONECTION_VOLLEY:
                if(mUrl.getText().toString().isEmpty())
                    Toast.makeText(Browser_Activity.this, "No hay una ruta establecida", Toast.LENGTH_LONG).show();
                else{

                    if(redDisponible())
                        conectarVolley();
                    else
                        Toast.makeText(Browser_Activity.this, "No hay una conexión disponible", Toast.LENGTH_LONG).show();
                }
                break;
        }
    }

    private boolean redDisponible(){
        boolean result = false;
        ConnectivityManager cm = (ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = cm.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected())
            result = true;

        return result;

    }

    //endregion

    //region AAHC

    private void conectarAAHC() {
        String texto = mUrl.getText().toString();
        final ProgressDialog progress = new ProgressDialog(this);

        AsyncHttpClient client = new AsyncHttpClient();
        client.get(texto, new TextHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {

                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                progress.dismiss();
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {

                progress.dismiss();
                mWebView.loadDataWithBaseURL(null, responseString, "text/html", ENCODING, null);
                contentWeb = new StringBuilder(responseString);
            }

            @Override
            public void onStart() {
                // called before request is started
                progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                progress.setMessage("Conectando . . .");
                progress.setCancelable(false);
                progress.show();
            }
        });
    }

    //endregion

    //region VOLLEY
    private void conectarVolley(){

        RequestQueue queue = Volley.newRequestQueue(this);
        final ProgressDialog progress = new ProgressDialog(this);
        String url = mUrl.getText().toString();
        progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progress.setMessage("Conectando . . .");
        progress.setCancelable(false);
        progress.show();
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // Display the first 500 characters of the response string.
                        progress.dismiss();
                        mWebView.loadDataWithBaseURL(null, response, "text/html", ENCODING, null);
                        contentWeb = new StringBuilder(response);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                progress.dismiss();
                Toast.makeText(Browser_Activity.this, error.toString(), Toast.LENGTH_LONG).show();
            }
        });

        queue.add(stringRequest);
    }

    //endregion

    //region JAVA

    class ConexionAsync extends AsyncTask<String, Integer, Resultado> {


        private ProgressDialog progreso;
        private Context context;
        private String encoding;
        public ConexionAsync(Context context, String encoding){

            this.context = context;
            this.encoding = encoding;
        }

        protected void onPreExecute() {
            progreso = new ProgressDialog(context);
            progreso.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progreso.setMessage("Conectando . . .");
            progreso.setCancelable(true);
            progreso.setOnCancelListener(new DialogInterface.OnCancelListener() {
                public void onCancel(DialogInterface dialog) {
                    ConexionAsync.this.cancel(true);
                }
            });
            progreso.show();
        }

        @Override
        protected Resultado doInBackground(String... params) {
            int i = 1;
            Resultado result = new Resultado();
            publishProgress(i);
            result = conectarJava(params[0]);
            return result;
        }

        private String read(InputStream entrada){

            BufferedReader in;
            String linea;
            StringBuilder miCadena = new StringBuilder();
            in = new BufferedReader(new InputStreamReader(entrada), 32000);
            try {
                while ((linea = in.readLine()) != null)
                    miCadena.append(linea);
            } catch (IOException e) {
                e.printStackTrace();
            }

            try {
                in.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return miCadena.toString();
        }

        private Resultado conectarJava(String texto){

            URL url;
            HttpURLConnection urlConnection = null;
            int respuesta;
            Resultado resultado = new Resultado();
            try {
                url = new URL(texto);
                urlConnection = (HttpURLConnection) url.openConnection();
                respuesta = urlConnection.getResponseCode();
                if (respuesta == HttpURLConnection.HTTP_OK) {
                    resultado.setCodigo(true);
                    resultado.setContenido(read(urlConnection.getInputStream()));


                } else {
                    resultado.setCodigo(false);
                    resultado.setMensaje("Error en el acceso a la web: " + String.valueOf(respuesta));
                }
            } catch (IOException e) {
                resultado.setCodigo(false);
                resultado.setMensaje("Excepción: " + e.getMessage());
            } finally {
                try {
                    if (urlConnection != null)
                        urlConnection.disconnect();
                } catch (Exception e) {
                    resultado.setCodigo(false);
                    resultado.setMensaje("Excepción: " + e.getMessage());
                }

            }

            return resultado;
        }

        @Override
        protected void onPostExecute(Resultado resultado) {
            progreso.dismiss();

            if(resultado.isCodigo()){

                mWebView.loadDataWithBaseURL(null, resultado.getContenido(), "text/html", encoding, null);
                StringBuilder sb = new StringBuilder("");
                sb.append(resultado.getContenido());
                contentWeb = sb;
            }

        }

        protected void onProgressUpdate(Integer... progress) {
            progreso.setMessage("Conectando " + Integer.toString(progress[0]));
        }
    }

    //endregion
}