package com.jsw.ejerciciosficheros;

import android.graphics.Color;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.CalendarView;
import android.widget.TextView;

import com.github.sundeepk.compactcalendarview.CompactCalendarView;
import com.github.sundeepk.compactcalendarview.domain.Event;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class MenstruApp_Mes_Activity extends AppCompatActivity {

    private CompactCalendarView mCalendar;
    private TextView mTexto;
    Date d;
    boolean cuidado = false;
    File testDirectory;
    File file;
    Calendar c;
    Date[] dates = new Date[5];

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menstru_app_mes);
        mTexto = (TextView)findViewById(R.id.textView7);
        c = Calendar.getInstance();
        mCalendar = (CompactCalendarView) findViewById(R.id.calendarView);
        String[] events = (String[]) getIntent().getExtras().get("events");
        testDirectory = Environment.getExternalStorageDirectory();
        file = new File(testDirectory + "/" + "fertiles.txt");

        for (int i = 0; i < events.length; i++) {
            mCalendar.addEvent(new Event(Color.RED, Long.valueOf(events[i])));
            d = new Date(Long.valueOf(events[i]));
            dates[i] = d;
            if(d.getDay() == c.getTime().getDay())
                cuidado = true;
        }

        escribir(dates);

        if (cuidado) {
            mTexto.setText("¡Cuidado! ¡Estás en tu día fértil!");
            mTexto.setVisibility(View.VISIBLE);
        }
    }

    private void escribir(Date[] d){
        BufferedWriter bw;

        try{

            file.createNewFile();

            bw = new BufferedWriter(new FileWriter(testDirectory + "/" + file.getName()));

            for (int i = 0; i < d.length; i++){
                bw.append(d[i].toString());
                bw.newLine();
                bw.flush();
            }

            if (bw != null)
                bw.close();
        }

        catch (Exception e) {
        Log.e("ERROR WRITING", "Unable to write" + e.getMessage());
        }
    }
}

