package com.jsw.ejerciciosficheros.adapter;

import android.content.Context;
import android.database.DataSetObserver;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.jsw.ejerciciosficheros.R;
import com.jsw.ejerciciosficheros.dao.daoAgenda;
import com.jsw.ejerciciosficheros.model.Contacto;

import java.util.List;

/**
 * Created by usuario on 21/10/16.
 */

public class adapterAgenda extends ArrayAdapter<Contacto> {


    public adapterAgenda(Context context) {
        super(context, R.layout.cards_layout, ((daoAgenda)context.getApplicationContext()).getContacts());
    }

    public void añadir(Contacto contacto){
        ((daoAgenda) getContext().getApplicationContext()).addAndWrite(contacto);
    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View item=convertView;
        ProductHolder productholder;

        //si el item ya tiene memoria lo usa.
        //utiliza la misma zona de memoria, la reutiliza
        if(item==null) {
            //1.crear un objeto inflaer que incicializamos al Layout inflater del contexto
            LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);


            //2.inflar la vista . crear en memoria el objeto view que contienen los widgets
            item = inflater.inflate(R.layout.cards_layout, null);

            //asignar memoria al holder
            productholder= new ProductHolder();

            //3.vinculado con el item(view), que se acaba de crear, con las variables del holder
            //con las direcciones de memoria de lo que se esta visualizando
            productholder.imgProd=(ImageView) item.findViewById(R.id.iv_photo);
            productholder.txvPriceCA=(TextView)item.findViewById(R.id.tv_name);
            productholder.txvNameCA=(TextView)item.findViewById(R.id.tv_phone);
            productholder.txvStockCA=(TextView)item.findViewById(R.id.tv_mail);;

            item.setTag(productholder);

        }
        else{
            //en caso de que se tenga un objeto se reutiliza
            productholder=(ProductHolder) item.getTag();
        }
        //4 asignar datos del adapter a los widget
        productholder.imgProd.setImageResource(R.mipmap.ic_launcher);
        productholder.txvNameCA.setText(getItem(position).getNombre());
        productholder.txvPriceCA.setText(String.valueOf(getItem(position).getNumero()));
        productholder.txvStockCA.setText(getItem(position).getMail());

        return item;
    }

    @Override
    public void registerDataSetObserver(DataSetObserver observer) {
        super.registerDataSetObserver(observer);
    }

    /**
     * Clase interna con la memoria hecha
     * */
    class ProductHolder{
        ImageView imgProd;
        TextView txvStockCA,txvNameCA,txvPriceCA;
    }
}